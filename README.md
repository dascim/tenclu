# TenClu

## Compile

```bash
~$ sbt assembly
```

## Use


### Louvain

```bash
~$ spark-submit <spark_parameters> 
       --class louvain.Main
       ./target/scala-2.11/ten_cent-assembly-0.0.1.jar
       -i <value> | --input <value>
           input file or path Required
       -o <value> | --output <value>
           output path Required
       -m, --map_file <value> 
             Defines the position and the name of the map file (in the DFS). If a map file exists it uses the existing one. If no name is given the the same is done on the default output location which is the one followed by "<output_path_name>_map.csv"
       -n <value> | --jobname <value>
           job name. Default=tenclu_louvain
       -x <value> | --minprogress <value>
           Number of vertices that must change communites for the algorithm to consider progress. Default=2000
       -y <value> | --progresscounter <value>
           Number of times the algorithm can fail to make progress before exiting. Default=1
       <property>=<value>....
```


### olp

```bash
~$ spark-submit <spark_parameters> 
       --class olp.Main
       ./target/scala-2.11/ten_cent-assembly-0.0.1.jar
       -i, --input <value>      input file or path  Required.
       -o, --output <value>     output path Required
       -m, --map_file <value>   Defines the position and the name of the map file (in the DFS). If a map file exists it uses the existing one. If no name is given the the same is done on the default output location which is the one followed by "<output_path_name>_map.csv"
       -d, --simple             defines if the simple label propagation will be executed
       -k, --kcore <distributed_boolean_flag>
             remove edge directions that propagate labels to higher cores: define if k core will be applied distributed
       -n, --jobname <value>    job name. Default=tenclu_olp
       -c, --cluster_size_weights <mu>,<sigma>[,<normalization-factor>]
             define if weighting will e applied based on the cluster size and define mu, sigma parameters for a normalized gaussian of values between 0 and 1 (on median) and a normalization factor bigger than 0 (default:<normalization-factor>=1.0
       -s, --max_steps <value>  the maximum number of iterations for the label propagation scheme. Default=20
       -p, --num_edge_partitions <value>
             number of edge partitions. Default=60
       -t, --threshold <value>  the threshold value for including a label inside the label set during label propagation. Default=0.001
       -l, --log_steps          print step information at each iteration
       -z, --max_k <value>      The maximum number of cluster contribution allowed in each label propagation. If smaller there is no upper limit. Default=-1
       <property>=<value>....
```

### Louvain/OLP

```bash
~$ spark-submit <spark_parameters> 
       --class LVolp.Main
       ./target/scala-2.11/ten_cent-assembly-0.0.1.jar
       -i, --input <value>      input file or path  Required.
       -o, --output <value>     output path Required
       -d, --simple             defines if the simple label propagation will be executed
       -m, --map_file <value> 
            Defines the position and the name of the map file (in the DFS). If a map file exists it uses the existing one. If no name is given the the same is done on the default output location which is the one followed by "<output_path_name>_map.csv"
       -k, --kcore <distributed_boolean_flag> 
            Remove edge directions that propagate labels to higher cores: define if k core will be applied distributed
       -n, --jobname <value>    job name. Default=tenclu_olp
       -c, --cluster_size_weights <mu>,<sigma>[,<normalization-factor>]
            define if weighting will e applied based on the cluster size and define mu, sigma parameters for a normalized gaussian of values between 0 and 1 (on median) and a normalization factor bigger than 0 (default:<normalization-factor>=1.0
       -s, --max_steps_lp <value> the maximum number of iterations for the label propagation scheme. Default=20
       -r, --max_hier <value>
            the maximum number of graph compressions allowed to be repeated by the Louvain part of the algorithm. Default=-1
       -p, --num_edge_partitions <value> number of edge partitions. Default=60
       -x, --minprogress <value> Number of vertices that must change communites for the algorithm to consider progress. Default=2000
       -y, --progresscounter <value> Number of times the algorithm can fail to make progress before exiting. Default=1
       -t, --threshold <value>  the threshold value for including a label inside the label set during label propagation. Default=0.01
       -l, --log_steps         print step information at each iteration
       -z, --max\_k <value> The maximum number of cluster contribution allowed in each label propagation. If smaller there is no upper limit. Default=-1
       <property>=<value>....
```

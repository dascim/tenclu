package graph

import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import scala.collection.immutable.IntMap
import scala.math.abs
import org.apache.spark.storage.StorageLevel
import org.apache.spark.broadcast.Broadcast

object KCoreD {
    val max_iter = 20
    def run(A: Graph[_, _], sc: SparkContext): Map[VertexId, Int] = {
        
        val B = A.edges.flatMap({x => Seq((x.srcId, x.dstId),(x.dstId, x.srcId))})

        val C = B.map({case (a, b) => (a, Seq(b))}).aggregateByKey(Seq.empty[Long])(_ ++ _, _ ++ _).map({case (a, b) => (a, b.toSet)}) // neighbors per node

        val degrees = C.map({case (a, b) => (a, b.size)})

        var degrees_all = sc.broadcast(degrees.collect.toMap)

        val D = C.map({case (a, b) => (a, b, degrees_all.value(a))})

        /* the following logic:
         * every node starts with the core number equal to its degrees
         * at every loop the core number is equal to the number of heighbors K that have at least
         * K core number currently the core numbers are updated at the end and broadcasted again 
         * for the next loop stop when there are no more updates 
         */

        def new_core(bv: Broadcast[Map[VertexId, Int]])(node_info: (VertexId, Set[VertexId], Int)): (VertexId, Set[VertexId], Int) = {
            // in descending order
	        val neigh_degrees = node_info._2.map(bv.value(_))(scala.collection.breakOut).sortWith(_ < _)
            return (node_info._1,
                    node_info._2,
                    neigh_degrees.foldLeft((0, 0))({ case ((index, i), v) => if (v >= i+1) (i+1, i+1) else (index, i+1)})._1)
        }

        var update = D.map(new_core(degrees_all)(_))
        var new_index = update.map(x => (x._1, x._3))
        var diff = update.aggregate(0)({case (s, x) => s + abs(x._3 - degrees_all.value(x._1))},
                                       {case (su, sv) => su + sv})
        var loop_count = 0
        while (diff > 0 || loop_count < max_iter){ // if it runs too long put a threshold of #max_iter loops
	        degrees_all = sc.broadcast(new_index.collect.toMap)
	        update = update.map(new_core(degrees_all)(_))
	        new_index = update.map(x => (x._1, x._3))
            diff = update.aggregate(0)({case (s, x) => s + abs(x._3 - degrees_all.value(x._1))},
                                       {case (su, sv) => su + sv})
            loop_count += 1
	    }

        return new_index.collect.toMap
    }
}

package graph

import java.io.IOException
import java.io.PrintWriter
import java.nio.file.{Paths, Files}
import scala.io.Source
import org.apache.spark.SparkContext
import org.apache.spark.graphx.{Graph, Edge}
import scala.util.MurmurHash
import org.apache.spark.SparkContext
import org.apache.spark.graphx._

object GraphUtilities{
    def graph_fromFile_mcl(filename: String, map_output_filename: String, ws: Int, sc: SparkContext, nP: Int) : Graph[None.type, Double] = {
        // Load Hadoop Configuration
        val conf = sc.hadoopConfiguration
        val fs = org.apache.hadoop.fs.FileSystem.get(conf)
        
        // Load Implicits concerning Dataframe creation
        val sqlContext= new org.apache.spark.sql.SQLContext(sc)
        import sqlContext.implicits._


        // Read Map to dataframe
        var save = false
        val ids = if (fs.exists(new org.apache.hadoop.fs.Path(map_output_filename))){
                        sc.textFile(map_output_filename, nP)
                          .map(line => {val line_list = line.split(",").map(_.trim);
                                        (line_list(1).toString,line_list(0).toLong)})
                          .toDF("oid","nid")

                  }else{
                       save = true
                       val sfileM = sc.textFile(filename, nP)
                       sfileM.filter(line => line.size > 0)
                             .flatMap(line => line.split(",").take(2).map(_.trim))
                             .distinct.zipWithIndex
                             .toDF("oid","nid")
                  }

        // Save if needed
        if (save){
            ids.select($"nid", $"oid").write.csv(map_output_filename);
        }

        val sfile = ws match {
                      case 0 => sc.textFile(filename, nP)
                                  .map(line => {val line_list = line.split(",").map(_.trim);
                                                (line_list(0),line_list(1), 1.0)})
                                  .toDF("from","to", "ew");
                      case 1 => sc.textFile(filename, nP)
                                  .map(line => {val line_list = line.split(",").map(_.trim);
                                                (line_list(0),line_list(1), line_list(2).toDouble)})
                                  .toDF("from","to", "ew"); 
                      case 2 => sc.textFile(filename, nP)
                                  .map(line => {val line_list = line.split(",").map(_.trim);
                                                (line_list(0),line_list(1), line_list(3).toDouble)})
                                  .toDF("from","to", "ew"); 
                      case 3 => sc.textFile(filename, nP)
                                  .map(line => {val line_list = line.split(",").map(_.trim);
                                                (line_list(0),line_list(1), line_list(3).toDouble/line_list(2).toDouble)})
                                  .toDF("from","to", "ew"); 
                    }

        var edges = sfile.join(ids, $"from" === $"oid" )
                         .toDF("fromO","to", "ew", "oidO", "from")
                         .drop("fromO")
                         .drop("oidO")
                         .join(ids, $"to" === $"oid" )
                         .toDF("toO", "ew", "from", "oidO", "to")
                         .select($"from",$"to",$"ew");

        return Graph.fromEdges(edges.rdd.map(x => Edge(x(0).toString.toLong, x(1).toString.toLong, x(2).toString.toDouble)), None);
    }

    def graph_fromFile_louvain(filename: String, map_output_filename: String, uta: Boolean, sc: SparkContext, nP: Int) : Graph[None.type, Long] = {
        // Load Hadoop Configuration
        val conf = sc.hadoopConfiguration
        val fs = org.apache.hadoop.fs.FileSystem.get(conf)

        // Load Implicits concerning Dataframe creation
        val sqlContext= new org.apache.spark.sql.SQLContext(sc)
        import sqlContext.implicits._

        // Read Map to dataframe
        var save = false
        val ids = if (fs.exists(new org.apache.hadoop.fs.Path(map_output_filename))){
                        sc.textFile(map_output_filename, nP)
                          .map(line => {val line_list = line.split(",").map(_.trim);
                                        (line_list(1).toString,line_list(0).toLong)})
                          .toDF("oid","nid")

                  }else{
                       save = true
                       val sfileM = sc.textFile(filename, nP)
                       sfileM.filter(line => line.size > 0)
                             .flatMap(line => line.split(",").take(2).map(_.trim))
                             .distinct.zipWithIndex
                             .toDF("oid","nid")
                  }

        // Save if needed
        if (save){
            ids.select($"nid", $"oid").write.csv(map_output_filename);
        }

        val sfile = if (uta){
                      sc.textFile(filename, nP)
                        .map(line => {val line_list = line.split(",").map(_.trim);
                                      (line_list(0),line_list(1), 1L)})
                        .toDF("from", "to", "ew");
                    }else{
                      sc.textFile(filename, nP)
                        .map(line => {val line_list = line.split(",").map(_.trim);
                                      (line_list(0), line_list(1), line_list(3))})
                        .toDF("from", "to", "ew");
                    };

        var edges = sfile.join(ids, $"from" === $"oid" )
                         .toDF("fromO","to", "ew", "oidO", "from")
                         .drop("fromO")
                         .drop("oidO")
                         .join(ids, $"to" === $"oid" )
                         .toDF("toO", "ew", "from", "oidO", "to")
                         .select($"from",$"to",$"ew");

        return Graph.fromEdges(edges.rdd.map(x => Edge(x(0).toString.toLong, x(1).toString.toLong, x(2).toString.toDouble.toLong)), None);
    }
}

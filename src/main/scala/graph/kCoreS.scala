package graph

import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import scala.math.abs
import org.apache.spark.storage.StorageLevel
import org.apache.spark.broadcast.Broadcast
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.{Set => MSet}
import scala.collection.mutable.{Map => MMap}

object KCoreS {
    def run(G: Graph[_, _], sc: SparkContext): Map[VertexId, Int] = {

        val deg: Map[VertexId, Int] = G.degrees.collect.toMap
        // sort nodes by degree
        val nodes: Array[VertexId] = deg.keys.toArray.sortWith(deg(_) > deg(_))
        var bin_boundaries: ListBuffer[Int] = ListBuffer(0)
        var curr_degree: Int = 0
        for ((v, i) <- nodes.zip(0 to nodes.length)){
            if (deg(v) > curr_degree){
                bin_boundaries ++= (1 to (deg(v)-curr_degree)).map(_ => i)
                curr_degree = deg(v)
            }
        }
        var node_pos: MMap[VertexId, Int] = MMap(nodes.zip(0 to nodes.length): _*)
        var core: MMap[VertexId, Int] = MMap(deg.toSeq: _*)
        var nbrs: Map[VertexId, MSet[VertexId]] = G.collectNeighborIds(EdgeDirection.Either).mapValues(a => MSet(a: _*)).collect.toMap
        var bin_boundariesA: Array[Int] = bin_boundaries.toArray
        for (v <- nodes){
            for (u <- nbrs(v)){
                if (core(u) > core(v)){
                    nbrs(u).remove(v)
                    val pos = node_pos(u)
                    val bin_start = bin_boundariesA(core(u))
                    node_pos(u) = bin_start
                    node_pos(nodes(bin_start)) = pos
                    val tmp = nodes(bin_start)
                    nodes(bin_start) = nodes(pos)
                    nodes(pos) = tmp
                    bin_boundariesA(core(u)) += 1
                    core(u)-=1
                }
            }
        }
        return Map(core.toSeq: _*)
    }
}

package kBetweenness

import java.util.Calendar

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import com.soteradefense.dga.graphx.louvain._

import graph.{GraphUtilities}
import com.centrality.kBC.KBetweenness

// specify command line options and their defaults
case class Config(
    input:String = "",
    output: String = "",
    appName: String="tenclu_kBetweeness",
    k: Int = 2,
    canonicalOrientation : Boolean = true,
    numEdgePartitions: Int = 60,
    weightingScheme: Int = 0,
    map_file: String = "",
    properties:Seq[(String,String)]= Seq.empty[(String,String)] )


/**
 * Execute the louvain distributed community detection.
 * Requires an edge file and output directory in hdfs (local files for local mode only)
 */
object Main {
  // Disable Spark messages when running program
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)

  def main(args: Array[String]) {
    // Parse Command line options
    val parser = new scopt.OptionParser[Config](this.getClass().toString()){
      opt[String]('i',"input") action {(x,c)=> c.copy(input=x)}  text("input file or path  Required.")
      opt[String]('o',"output") action {(x,c)=> c.copy(output=x)} text("output path Required")
      opt[String]('n',"jobname") action {(x,c)=> c.copy(appName=x)} text("job name. Default=tenclu_kBetweeness")
      opt[Int]('k', "k") action {(x,c)=> c.copy(k=x)} text("k betweeners parameter. Default k=2")
      opt[String]('m',"map_file").action({(x,c) => c.copy(map_file=x)}).text("Defines the position and the name of the map file (in the DFS). If a map file exists it uses the existing one. If no name is given the the same is done on the default output location which is the one followed by \"<output_path_name>_map.csv\"")
      opt[Int]('w', "weighting_scheme") action {(x,c)=> c.copy(weightingScheme=x)} text("weighting scheme. Default=0")
      opt[Int]('p', "num_edge_partitions") action {(x,c)=> c.copy(numEdgePartitions=x)} text("number of edge partitions. Default=60")
      opt[Boolean]('c', "canonical_orientation") action {(x,c)=> c.copy(canonicalOrientation=x)} text("canonical orientation. Default=true")
      arg[(String,String)]("<property>=<value>....") unbounded() optional() action {case((k,v),c)=> c.copy(properties = c.properties :+ (k,v)) }
    }
    var inputFile,outputDir,jobname = ""
    var properties:Seq[(String,String)]= Seq.empty[(String,String)]
    var parallelism,minProgress,progressCounter = -1
    var k = 2
    var ipaddress = false
    var canonicalOrientation = true
    var numEdgePartitions = 60
    var weightingScheme = 0
    var map_file = ""
    parser.parse(args,Config()) map {
      config =>
        inputFile = config.input
        outputDir = config.output
        jobname = config.appName
        k = config.k
        canonicalOrientation = config.canonicalOrientation
        numEdgePartitions = config.numEdgePartitions
        weightingScheme = config.weightingScheme
        map_file = config.map_file
        if (weightingScheme > 3 || weightingScheme < 0){
           println("Wrong weighting scheme input! 0 for 1.0, 1 for A, 2 for B, 3 for B/A");
           sys.exit(1)
        }else if (inputFile == "" || outputDir == "") {
          println(parser.usage)
          sys.exit(1)
        }
    } getOrElse{
      sys.exit(1)
    }
    
    // set system properties
    properties.foreach( {case (k,v)=>
      println(s"System.setProperty($k, $v)")
      System.setProperty(k, v)
    })

    val conf = new SparkConf().setAppName(jobname);
    var sc: SparkContext = new SparkContext(conf)

    // Output params
    val inputFileName = inputFile.split("/").last.split('.').head;
    val DEFAULT_V_OUTPUT_FILE=List(inputFileName,"kbc",k,"vertices").mkString("_")+".txt"
    val DEFAULT_E_OUTPUT_FILE=List(inputFileName,"kbc",k,"edges").mkString("_")+".txt"

    val outputVerticesFileName = sc.hadoopConfiguration.get("outputVerticesFileName", DEFAULT_V_OUTPUT_FILE)
    val outputEdgesFileName = sc.hadoopConfiguration.get("outputEdgesFileName", DEFAULT_E_OUTPUT_FILE)
    val outputVerticesPath = sc.hadoopConfiguration.get("outputVerticesPath", outputDir+outputVerticesFileName)
    val outputEdgesPath = sc.hadoopConfiguration.get("outputEdgesPath", outputDir+outputEdgesFileName)

    println("outputVerticesPath : " + outputVerticesPath)
    println("outputEdgesPath : " + outputEdgesPath)
    
    // Read graph
    println("Building Graph..")
    if (map_file == ""){
        var parent_dir = outputDir.split("/").dropRight(1).mkString("/")
        if (parent_dir.isEmpty){
           parent_dir = "."
        }
        map_file = parent_dir  + "/" + outputDir.split("/").last.split('.').head+"_map.csv"
    }
    val graph = GraphUtilities.graph_fromFile_mcl(inputFile, map_file, weightingScheme, sc, numEdgePartitions).partitionBy(PartitionStrategy.EdgePartition2D)
    println(Calendar.getInstance().getTime().toString + " vertices : " + graph.vertices.count())
    println(Calendar.getInstance().getTime().toString + " edges : " + graph.edges.count())
    
    // Run kBC
    println(Calendar.getInstance().getTime().toString + ": start kBC")
    val kBCGraph = 
      KBetweenness.run(graph, k)
    
    // Save graph to file
    println(Calendar.getInstance().getTime().toString + ": saving results ") 
    kBCGraph.vertices.coalesce(1).saveAsTextFile(outputVerticesPath)
    kBCGraph.edges.coalesce(1).saveAsTextFile(outputEdgesPath)
      }
}




package louvain

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import com.soteradefense.dga.graphx.louvain._

import graph.{GraphUtilities}

// specify command line options and their defaults
case class Config(
    input:String = "",
    output: String = "",
    appName:String="tenclu_louvain",
    minProgress:Int = 2000,
    progressCounter:Int = 1,
    numEdgePartitions: Int = 60,
    map_file: String = "",
    uta: Boolean=false,
//    weighting_scheme: Int = 0,
    properties:Seq[(String,String)]= Seq.empty[(String,String)] )


/**
 * Execute the louvain distributed community detection.
 * Requires an edge file and output directory in hdfs (local files for local mode only)
 */
object Main {
  // Disable Spark messages when running program
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)

  def main(args: Array[String]) {
    // Parse Command line options
    val parser = new scopt.OptionParser[Config](this.getClass().toString()){
      opt[String]('i',"input") action {(x,c)=> c.copy(input=x)}  text("input file or path Required")
      opt[String]('o',"output") action {(x,c)=> c.copy(output=x)} text("output path Required")
      opt[String]('n',"jobname") action {(x,c)=> c.copy(appName=x)} text("job name. Default=tenclu_louvain")
      opt[String]('m',"map_file").action({(x,c) => c.copy(map_file=x)}).text("Defines the position and the name of the map file (in the DFS). If a map file exists it uses the existing one. If no name is given the the same is done on the default output location which is the one followed by \"<output_path_name>_map.csv\"")
      opt[Int]('x',"minprogress") action {(x,c)=> c.copy(minProgress=x)} text("Number of vertices that must change communites for the algorithm to consider progress. Default=2000")
      opt[Int]('y',"progresscounter") action {(x,c)=> c.copy(progressCounter=x)} text("Number of times the algorithm can fail to make progress before exiting. Default=1")
      opt[Unit]('a',"transaction_amount") action {(_, c) => c.copy(uta=true)} text("defines if we will use edge weights that correspond to the transaction amount")
      opt[Int]('p', "num_edge_partitions") action {(x,c)=> c.copy(numEdgePartitions=x)} text("number of edge partitions. Default=60")
      arg[(String,String)]("<property>=<value>....") unbounded() optional() action {case((k,v),c)=> c.copy(properties = c.properties :+ (k,v)) }
    }
    var edgeFile,outputdir,jobname = ""
    var properties:Seq[(String,String)]= Seq.empty[(String,String)]
    var minProgress,progressCounter = -1
    var numEdgePartitions = 60
    var map_file = ""
    var uta = false
//    var weighting_scheme = 0
    parser.parse(args,Config()) map {
      config =>
        edgeFile = config.input
        outputdir = config.output
        jobname = config.appName
        properties = config.properties
        minProgress = config.minProgress
        progressCounter = config.progressCounter
        numEdgePartitions = config.numEdgePartitions
        map_file = config.map_file
        uta = config.uta
//        weighting_scheme = config.weighting_scheme
        if (edgeFile == "" || outputdir == "") {
          println(parser.usage)
          sys.exit(1)
        }
    } getOrElse{
      sys.exit(1)
    }
    
    // set system properties
    properties.foreach( {case (k,v)=>
      println(s"System.setProperty($k, $v)")
      System.setProperty(k, v)
    })
    
    // Create the spark context
    println("Creating Context..")
    val conf = new SparkConf().setAppName(jobname);
    var sc: SparkContext = new SparkContext(conf)
    sc.setLogLevel("WARN")
   
    // create the graph
    println("Building Graph..")
    if (map_file == ""){
        var parent_dir = outputdir.split("/").dropRight(1).mkString("/")
        if (parent_dir.isEmpty){
           parent_dir = "."
        }
        map_file = parent_dir  + "/" + outputdir.split("/").last.split('.').head+"_map.csv"
    }
    val graph = GraphUtilities.graph_fromFile_louvain(edgeFile, map_file, uta, sc, numEdgePartitions).partitionBy(PartitionStrategy.EdgePartition2D)

    // Abort lazyness
    println("Graph Created: [#" + graph.vertices.count.toString + "V, #" + graph.edges.count.toString + "E]")

    // use a helper class to execute the louvain
    // algorithm and save the output.
    // to change the outputs you can extend LouvainRunner.scala
    println("Clustering..")
    val runner = new HDFSLouvainRunner(minProgress,progressCounter,outputdir)
    runner.run(sc, graph)
    println("Finished..")
  }
}




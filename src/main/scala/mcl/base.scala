package mcl

import java.io.IOException
import java.nio.file.{Paths, Files}

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.graphx._
import org.apache.spark.mllib.clustering.{Assignment, MCL}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.functions.{sort_array,collect_list,col}

import graph.{GraphUtilities}

object Test{
   def main(args: Array[String]): Unit = {
        val logFile = "./log.txt";
        val conf = new SparkConf().setAppName("mcl");
        val sc = new SparkContext(conf);

        val users: RDD[(VertexId, String)] =
            sc.parallelize(Array((0L,"Node1"), (1L,"Node2"),
                (2L,"Node3"), (3L,"Node4"),(4L,"Node5"),
                (5L,"Node6"), (6L,"Node7"), (7L, "Node8"),
                (8L, "Node9"), (9L, "Node10"), (10L, "Node11")));

        // Create an RDD for edges
        val relationships: RDD[Edge[Double]] =
                    sc.parallelize(
                      Seq(Edge(0, 1, 1.0), Edge(1, 0, 1.0),
                        Edge(0, 2, 1.0), Edge(2, 0, 1.0),
                        Edge(0, 3, 1.0), Edge(3, 0, 1.0),
                        Edge(1, 2, 1.0), Edge(2, 1, 1.0),
                        Edge(1, 3, 1.0), Edge(3, 1, 1.0),
                        Edge(2, 3, 1.0), Edge(3, 2, 1.0),
                        Edge(4, 5, 1.0), Edge(5, 4, 1.0),
                        Edge(4, 6, 1.0), Edge(6, 4, 1.0),
                        Edge(4, 7, 1.0), Edge(7, 4, 1.0),
                        Edge(5, 6, 1.0), Edge(6, 5, 1.0),
                        Edge(5, 7, 1.0), Edge(7, 5, 1.0),
                        Edge(6, 7, 1.0), Edge(7, 6, 1.0),
                        Edge(3, 8, 1.0), Edge(8, 3, 1.0),
                        Edge(9, 8, 1.0), Edge(8, 9, 1.0),
                        Edge(9, 10, 1.0), Edge(10, 9, 1.0),
                        Edge(4, 10, 1.0), Edge(10, 4, 1.0)
                      ));

        // Build the initial Graph
        val graph = Graph(users, relationships);
        graph.cache();

        val clusters: Dataset[Assignment] =
            MCL.train(graph).assignments
        clusters
            .groupBy("cluster")
            .agg(sort_array(collect_list(col("id"))))
            .show(3);

    }
}

object Main{
  // Disable Spark messages when running program
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)

  case class Config(
      input:String = "",
      output: String = "",
      appName: String="tenclu_mcl",
      expansionRate: Int=2,
      inflationRate: Double=2.0,
      epsilon: Double=0.01,
      maxIterations: Int=10,
      selfLoopWeight: Double=1.0,
      graphOrientationStrategy: String="undirected",
      weightingScheme: Int=0,
      numEdgePartitions: Int=60,
      map_file: String = "",
      properties:Seq[(String,String)]= Seq.empty[(String,String)] )


  @throws(classOf[IOException])
  def main(args: Array[String]): Unit = {
    try{
    // Manage options for the program
    val parser = new scopt.OptionParser[Config](this.getClass().toString()){
          opt[String]('i',"input") action {(x,c)=> c.copy(input=x)} text("input path Required")
          opt[String]('o',"output") action {(x,c)=> c.copy(output=x)} text("output path Required")
          opt[String]('m',"map_file").action({(x,c) => c.copy(map_file=x)}).text("Defines the position and the name of the map file (in the DFS). If a map file exists it uses the existing one. If no name is given the the same is done on the default output location which is the one followed by \"<output_path_name>_map.csv\"")
          opt[String]('n',"jobname") action {(x,c)=> c.copy(appName=x)} text("job name. Default=tenclu_mcl")
          opt[Int]('x',"expansionRate") action {(x,c)=> c.copy(expansionRate=x)} text("The rate in which clustering expands by connecting nodes to further and new parts of the graph. Default=2")
          opt[Int]('f',"inflationRate") action {(x,c)=> c.copy(inflationRate=x)} text("The rate in which intra cluster links are increased and inter cluster links decreased. Default=2.0")
          opt[Double]('e', "epsilon") action {(x,c)=> c.copy(epsilon=x)} text("Edges whose weight is inferior the epsilon times the sum of every edges weight connected to each node will be removed. Default=0.01")
          opt[Int]('r',"maxIterations") action {(x,c)=> c.copy(maxIterations=x)} text("It forces MCL to stop if it hasn'y converged after this number of iterations. Default=10")
          opt[Double]('l',"selfLoopWeight") action {(x,c)=> c.copy(selfLoopWeight=x)} text("A percentage of the maximum weight can be applied to added self loop. Default=1.0")
          opt[String]('s',"graphOrientationStrategy") action {(x,c)=> c.copy(graphOrientationStrategy=x)} text("\"directed\", \"undirected\", \"bidirected\". Default=undirected")
          opt[Int]('w',"weightingScheme") action {(x,c)=> c.copy(weightingScheme=x)} text("0 for 1.0, 1 for A, 2 for B, 3 for B/A. Default=0")
          opt[Int]('p', "num_edge_partitions") action {(x,c)=> c.copy(numEdgePartitions=x)} text("number of edge partitions. Default=60")
          arg[(String,String)]("<property>=<value>....") unbounded() optional() action {case((k,v),c)=> c.copy(properties = c.properties :+ (k,v)) }
        }

        var inputFile,outputdir,jobname, graphOrientationStrategy = ""
        var maxIterations, expansionRate, weightingScheme = 0
        var inflationRate, epsilon, selfLoopWeight = 0.0
        var numEdgePartitions = 60
        var map_file = ""
        var properties:Seq[(String,String)]= Seq.empty[(String,String)]
        parser.parse(args,Config()) map {
          config =>
            inputFile = config.input
            outputdir = config.output
            jobname = config.appName
            expansionRate = config.expansionRate
            inflationRate = config.inflationRate
            epsilon = config.epsilon
            maxIterations = config.maxIterations
            selfLoopWeight = config.selfLoopWeight
            graphOrientationStrategy = config.graphOrientationStrategy
            weightingScheme = config.weightingScheme
            numEdgePartitions = config.numEdgePartitions
            map_file = config.map_file
            if (weightingScheme > 3 || weightingScheme < 0){
               println("Wrong weighting scheme input! 0 for 1.0, 1 for A, 2 for B, 3 for B/A");
               sys.exit(1)
            }else if (inputFile == "" || outputdir == "") {
              println(parser.usage)
              sys.exit(1)
            }
        } getOrElse{
          sys.exit(1)
        }

       // Initialize Spark Variables
       println("Creating Context..")
       val conf = new SparkConf().setAppName(jobname);
       val sc = new SparkContext(conf);
       sc.setLogLevel("WARN")

       // Get Graph  
       println("Building Graph..")
       if (map_file == ""){
           var parent_dir = outputdir.split("/").dropRight(1).mkString("/")
           if (parent_dir.isEmpty){
              parent_dir = "."
           }
           map_file = parent_dir  + "/" + outputdir.split("/").last.split('.').head+"_map.csv"
       }
       val graph: Graph[None.type, Double]  = GraphUtilities.graph_fromFile_mcl(
                inputFile, map_file, weightingScheme, sc, numEdgePartitions).partitionBy(PartitionStrategy.EdgePartition2D);
       graph.cache();

       println("Clustering..")
       val clusters: Dataset[Assignment] =
           MCL.train(
            graph,
            expansionRate,
            inflationRate,
            epsilon,
            maxIterations,
            selfLoopWeight,
            graphOrientationStrategy).assignments

       println("Saving with filename: " + outputdir)
       clusters
           .write
           .csv(outputdir)

   }catch{
      case e: Exception => println(e.getMessage)
        sys.exit(1)
   }
 }
}


package LVolp

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import org.apache.spark.graphx.lib.LabelPropagation
import org.apache.spark.sql.SparkSession
import com.soteradefense.dga.graphx.louvain._
import org.apache.spark.sql.SQLContext
import graph.{GraphUtilities}

// specify command line options and their defaults
case class Config(
    input:String = "",
    output: String = "",
    appName: String="tenclu_LVolp",
    maxsteps: Int=20,
    simple: Boolean=false,
    threshold: Double=0.01,
    with_ew: Boolean=false,
    with_kcore: Char='0',
    with_cw: Option[Seq[Double]] = None,
    numEdgePartitions: Int = 60,
    minProgress:Int = 2000,
    progressCounter:Int = 1,
    max_hier: Int = -1,
    log_steps: Boolean=false,
    map_file: String = "",
    ub: Int = -1,
    uta: Boolean=false,
    max_k: Int = -1,
    properties:Seq[(String,String)]= Seq.empty[(String,String)] )


/**
 * Execute the louvain distributed community detection.
 * Requires an edge file and output directory in hdfs (local files for local mode only)
 */
object Main {
  // Disable Spark messages when running program
  Logger.getLogger("org").setLevel(Level.WARN)
  Logger.getLogger("akka").setLevel(Level.WARN)

  def main(args: Array[String]) {
    // Parse Command line options
    val parser = new scopt.OptionParser[Config](this.getClass().toString()){
      opt[String]('i',"input").required.action({(x,c) => c.copy(input=x)}).text("input file or path  Required.")
      opt[String]('o',"output").required.action({(x,c) => c.copy(output=x)}).text("output path Required")
      opt[Unit]('d',"simple") action {(_, c) => c.copy(simple=true)} text("defines if the simple label propagation will be executed")
      opt[String]('m',"map_file").action({(x,c) => c.copy(map_file=x)}).text("Defines the position and the name of the map file (in the DFS). If a map file exists it uses the existing one. If no name is given the the same is done on the default output location which is the one followed by \"<output_path_name>_map.csv\"")
      opt[Unit]('w',"weight_edges") action {(_,c)=> c.copy(with_ew=true)} text("weight edges based on triangle count and degree")
      opt[Boolean]('k',"kcore").valueName("<distributed_boolean_flag>") action {(x,c)=> c.copy(with_kcore= if (x){'d'} else{'s'})} text("remove edge directions that propagate labels to higher cores: define if k core will be applied distributed")
      opt[String]('n',"jobname") action {(x,c)=> c.copy(appName=x)} text("job name. Default=tenclu_olp")
      opt[Seq[Double]]('c',"cluster_size_weights").valueName("<mu>,<sigma>[,<normalization-factor>]").action((x,c) =>
    c.copy(with_cw = Some(x))).text("define if weighting will e applied based on the cluster size and define mu, sigma parameters for a normalized gaussian of values between 0 and 1 (on median) and a normalization factor bigger than 0 (default:<normalization-factor>=1.0")
      opt[Int]('s',"max_steps_lp") action {(x,c)=> c.copy(maxsteps=x)} text("the maximum number of iterations for the label propagation scheme. Default=20")
      opt[Int]('r',"max_hier") action {(x,c)=> c.copy(max_hier=x)} text("the maximum number of graph compressions allowed to be repeated by the Louvain part of the algorithm. Default=-1")
      opt[Int]('p', "num_edge_partitions") action {(x,c)=> c.copy(numEdgePartitions=x)} text("number of edge partitions. Default=60")
      opt[Int]('x',"minprogress") action {(x,c)=> c.copy(minProgress=x)} text("Number of vertices that must change communites for the algorithm to consider progress. Default=2000")
      opt[Int]('y',"progresscounter") action {(x,c)=> c.copy(progressCounter=x)} text("Number of times the algorithm can fail to make progress before exiting. Default=1")
      opt[Double]('t',"threshold") action {(x,c)=> c.copy(threshold=x)} text("the threshold value for including a label inside the label set during label propagation. Default=0.01")
      opt[Unit]('l', "log_steps") action {(_,c)=> c.copy(log_steps=true)} text("print step information at each iteration")
      opt[Unit]('a',"transaction_amount") action {(_, c) => c.copy(uta=true)} text("defines if we will use edge weights that correspond to the transaction amount")
      opt[Int]('u', "cluster_size_upper_bound") action {(x,c)=> c.copy(ub=x)} text("the upper bound of the cluster sizes. Integer bigger equal to 1. If smaller no bounding is applied. Default=-1")
      opt[Int]('z', "max_k") action {(x,c)=> c.copy(max_k=x)} text("The maximum number of cluster contribution allowed in each label propagation. If smaller there is no upper limit. Default=-1")

      arg[(String,String)]("<property>=<value>....") unbounded() optional() action {case((k,v),c)=> c.copy(properties = c.properties :+ (k,v)) }
    }
    var edgeFile,outputdir,jobname = ""
    var threshold = 0.001
    var maxsteps = 20
    var simple = false
    var with_ew = false
    var with_kcore = '0'
    var map_file = ""
    var with_cw: Option[(Double, Double, Double, Int)] = None
    var numEdgePartitions = 60
    var minProgress,progressCounter = -1
    var log_steps = false
    var properties:Seq[(String,String)]= Seq.empty[(String,String)]
    var max_hier = -1
    var uta = false
    var ub = -1
    var max_k = -1
//    var weighting_scheme = 0
    parser.parse(args,Config()) map {
      config =>
        edgeFile = config.input
        outputdir = config.output
        jobname = config.appName
        properties = config.properties
        maxsteps = config.maxsteps
        threshold = config.threshold
        with_ew = config.with_ew
        with_kcore = config.with_kcore
        minProgress = config.minProgress
        progressCounter = config.progressCounter
        numEdgePartitions = config.numEdgePartitions
        max_hier = config.max_hier
        log_steps = config.log_steps
        map_file = config.map_file
        uta = config.uta
        max_k = config.max_k
        ub = config.ub
        config.with_cw match{
            case None => { with_cw = None }
            case Some(cw) => {
                if ((cw.length == 3) && 
                    (cw(0) >= 0.0 && cw(0) <= 1.0) && 
                    (cw(1) >= 0.0 && cw(1) <= 1.0) && 
                    (cw(2) > 0.0)){
                    with_cw = Some((cw(0), cw(1), cw(2), ub))
                }else if ((cw.length == 2) && 
                    (cw(0) >= 0.0 && cw(0) <= 1.0) && 
                    (cw(1) >= 0.0 && cw(1) <= 1.0)){
                    with_cw = Some((cw(0), cw(1), 1.0, ub))
                }
                else{
                  println("-c, -cluster_size_weights must be followed by 2-3 numbers indicating mu, sigma parameters for a normalized gaussian of values between 0 and 1 (on median) and a normalization factor bigger than 0")
                  sys.exit(1)
                }
            }
        }
        simple = config.simple
//        weighting_scheme = config.weighting_scheme
        if (edgeFile == "" || outputdir == "") {
          println(parser.usage)
          sys.exit(1)
        }
    } getOrElse{
      sys.exit(1)
    }
    
    // set system properties
    properties.foreach( {case (k,v)=>
      println(s"System.setProperty($k, $v)")
      System.setProperty(k, v)
    })
    
    // Create the spark context
    println("Creating Context..")
    val conf = new SparkConf().setAppName(jobname);
    val sc = new SparkContext(conf)    
    val sqlContext = new SQLContext(sc)
    import sqlContext.implicits._
    
    // create the graph
    println("Building Graph..")

    var parent_dir = outputdir.split("/").dropRight(1).mkString("/")
    if (parent_dir.isEmpty){
       parent_dir = "." 
    }
    if (map_file == ""){
        map_file = parent_dir  + "/" + outputdir.split("/").last.split('.').head +"_map.csv"
    }
    val graph = GraphUtilities.graph_fromFile_louvain(edgeFile, map_file, uta, sc, numEdgePartitions).partitionBy(PartitionStrategy.EdgePartition2D).cache()

    println("Clustering [Louvain (1/2)]..")
    val runner = new LouvainForLP(minProgress, progressCounter, max_hier, outputdir + "_louvain")
    val graphLV = runner.runG(sc, graph)

    // use a helper class to execute the louvain
    // algorithm and save the output.
    // to change the outputs you can extend LouvainRunner.scala

    val outputdir_olp = outputdir + "_olp"
    val base_filename = parent_dir  + "/" + outputdir_olp.split("/").last.split('.').head

    if (simple){
        println("Clustering [Label Propagation (2/2)]..")
        val DF = (LabelPropagation
                 .run(graphLV, maxsteps))
                 .vertices.mapValues(a => a.toString).toDF
        DF.write.csv(outputdir_olp)        
    }
    else{
        println("Clustering [Overlapping Label Propagation (2/2)]..")
        val DF = (LVOverlapingLabelPropagation
                 .run(graphLV, maxsteps, threshold, with_kcore, with_cw, with_ew, max_k, log_steps, base_filename, sc, sqlContext))
                 .vertices.mapValues(a => a.mkString(",")).toDF
        DF.write.csv(outputdir_olp)
    }
  }
}




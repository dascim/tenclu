package LVolp

import com.soteradefense.dga.graphx.louvain._

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.graphx._
import scala.reflect.ClassTag

/**
 * Execute the louvain algorithim and save the vertices and edges in hdfs at each level.
 * Can also save locally if in local mode.
 * 
 * See LouvainHarness for algorithm details
 */

class LouvainForLP(minProgress:Int, progressCounter:Int, max_hier: Int, outputdir:String) extends HDFSLouvainRunner(minProgress:Int, progressCounter:Int, outputdir:String){
  
  /* Override in order to return communities */
  def runG[VD: ClassTag](sc:SparkContext, graph:Graph[VD,Long]): Graph[VertexState,Long] = {
    
    var louvainGraph = LouvainCore.createLouvainGraph(graph)
    
    var level = -1  // number of times the graph has been compressed
	var q = -1.0    // current modularity value
	var halt = false
    do {
	  level += 1

	  println(s"\nStarting Louvain level $level")
	  
	  // label each vertex with its best community choice at this level of compression
	  val (currentQ,currentGraph,passes) = LouvainCore.louvain(sc, louvainGraph,minProgress,progressCounter)
	  louvainGraph.unpersistVertices(blocking=false)
	  louvainGraph=currentGraph
	  
	  saveLevel(sc,level,currentQ,louvainGraph)
	  
	  // If modularity was increased by at least 0.001 compress the graph and repeat
	  // halt immediately if the community labeling took less than 3 passes
	  //println(s"if ($passes > 2 && $currentQ > $q + 0.001 )")
	  if (passes > 2 && currentQ > q + 0.001 && level != max_hier){ 
	    q = currentQ
	    louvainGraph = LouvainCore.compressGraph(louvainGraph)
	  }
	  else {
	    halt = true
	  }
	 
	}while ( !halt )
	finalSave(sc,level,q,louvainGraph)
    return louvainGraph
  }
  
}

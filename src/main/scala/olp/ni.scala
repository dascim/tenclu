package olp
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
case class Index(clic:String)

object NI{
        
    def run(g: Graph[_, _], sc: SparkContext) : Map[VertexId, Double] = {
        /* Calculate triangle counts */
        val triCounts = sc.broadcast(g.triangleCount.vertices.collect.toMap)
        val degrees = sc.broadcast(g.degrees.collect.toMap)

        /* Calculate tri degrees */
        val tri_deg = g.vertices.map { case (vid, _) => (vid, degrees.value(vid) + triCounts.value(vid))}

        /* Normalize on [1/2,1] */
        val nf_mnpp = tri_deg.min()(Ordering.by({case (_, v) => v}))._2.toDouble
        val nf_mxpp = tri_deg.max()(Ordering.by({case (_, v) => v}))._2.toDouble
        //val nf_id_mnpm = g.vertices.min()(Ordering.by({case (vid, _) => triCounts.value(vid) - degrees.value(vid)}))._1
        //val nf_mnpm = (triCounts.value(nf_id_mnpm) - degrees.value(nf_id_mnpm)).toDouble
        return tri_deg.map({case (key, value) => (key, 0.5 + ((0.5*(value - nf_mnpp))/(nf_mxpp - nf_mnpp)))}).collect.toMap
    }
}

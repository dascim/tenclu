package olp

import scala.reflect.ClassTag
import scala.math.{exp, pow}
import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.SQLContext
import scalaz.Scalaz._
import java.io.PrintWriter

object OverlapingLabelPropagation {
  /* v : Attribution Factor */
  def run[VD: ClassTag, ED: ClassTag](g: Graph[VD, ED], maxSteps: Int, threshold: Double, with_kcore: Char, with_cw: Option[(Double, Double, Double, Int)], with_ew: Boolean, max_k: Int, log_steps: Boolean, base_filename:String, sc: SparkContext, sqlc: SQLContext): Graph[Map[VertexId, Double], ED] = {

    /* Calculate k core index for each node and keep only edges that are from a node with bigger equal k 
     * core number than the one the one it is pointing to.
     */
    val activeDirection = EdgeDirection.Out
    val sendMessage = if (with_kcore == '0'){
        (data: Any) => (e: EdgeContext[Map[VertexId, Double], ED, Map[VertexId, Double]]) => {
          /* Send your map to father and child */
          e.sendToSrc(e.dstAttr)
          e.sendToDst(e.srcAttr)
        }
    }else{
        (data: Any) => (e: EdgeContext[Map[VertexId, Double], ED, Map[VertexId, Double]]) => {
          /* Send your map to father and child */
          e.sendToDst(e.srcAttr)
        }
    }

    import sqlc.implicits._
    val printF = if (log_steps){
        (i: Int) => (ge: VertexRDD[Map[VertexId, Double]]) => {
            val DF = ge.mapValues(a => a.mkString(",")).toDF
            DF.write.csv(base_filename + "_steps_" + i.toString + ".csv")
        }
    }else{
        (i: Int) => (ge: VertexRDD[Map[VertexId, Double]]) => {}
    }

    def mergeMessage(data: Any)(vec_a: Map[VertexId, Double], vec_b: Map[VertexId, Double]) : Map[VertexId, Double] = {
      /* Merge two maps by adding values on common keys */
      return vec_a.unionWith(vec_b)(_ + _)
    }


    def mergeMaps(vec_a: Map[VertexId, Long], vec_b: Map[VertexId, Long]) : Map[VertexId, Long] = {
      return vec_a.unionWith(vec_b)(_ + _)
    }

    val take = if (max_k > 0){
            (m: Map[Long, Double]) => {
                if (m.size <= max_k){
                    m
                }else if(m.size >= 2* max_k){
                    var elem = Seq[(Long, Double)]()
                    var mm = collection.mutable.Map(m.toSeq: _*) 
                    for (i <- 1 to max_k){
                        val max_elem = mm.maxBy(_._2)
                        elem = elem :+ max_elem
                        mm -= max_elem._1
                    }
                    elem.toMap
                }else{
                    var mm = collection.mutable.Map(m.toSeq: _*) 
                    for (i <- 1 to (max_k - m.size)){
                       val min_elem = m.minBy(_._2)
                       mm -= min_elem._1
                    }
                    collection.immutable.Map(mm.toSeq: _*) 
              }
            }
        }else{
            (m: Map[Long, Double]) => { m }
        }

    val initialMessage = Map[VertexId, Double]()

    def normalize(message: Map[VertexId, Double], w: Double): Map[VertexId, Double] = {
        val mv: Double = message.values.sum;
        return if (mv == 0.0){
                    val s: Double = message.size.toDouble
                    message.transform((k, v) => w/s);
               }else{
                    message.transform((k, v) => (w*v)/mv);
               }
    }

    with_cw match{
        case None => {
            var initGraph = (if (with_kcore != '0'){
                                    var kci: Map[VertexId, Int] = Map[VertexId, Int]()
                                    if (with_kcore == 'd'){
                                         import graph.{KCoreD => KCore}
                                         kci = KCore.run(g, sc)
                                    }
                                    else if (with_kcore == 's'){
                                        import graph.{KCoreS => KCore}
                                        kci = KCore.run(g, sc)
                                    }else{
                                        throw new Exception("Unrecognized kCore argument")
                                    }

                                    def produce_edges[A: ClassTag](e: Edge[A]) : Seq[Edge[A]] = {
                                        if (kci(e.srcId) < kci(e.dstId)){
                                           return Seq(e)
                                        }else if (kci(e.srcId) > kci(e.dstId)){
                                           return Seq(Edge(e.dstId, e.srcId, e.attr))
                                        }else{
                                           return Seq(e, Edge(e.dstId, e.srcId, e.attr))
                                        }
                                    }
                                    val edges = g.edges.flatMap(produce_edges)
                                    /* Initially, each node x’s label set has only one element (x,1) */
                                    Graph(g.vertices.map({ case (vid, _) => (vid, Map(vid -> 1.0))}), edges).partitionBy(PartitionStrategy.EdgePartition2D).cache()
                                }else{
                                    g.mapVertices({ case (vid, _) => Map(vid -> 1.0)})
                                }).cache()

            val initialData = None

            def nonce(data: Any)(g: Graph[Map[VertexId, Double], ED]): None.type ={
                None
            }

            if (with_ew){
                /* Calculate normalized weigths for all nodes based on the degree count and triangle count */
                val nim: Map[VertexId, Double] = NI.run(g, sc)
                new PrintWriter(base_filename + "_NIweight.csv") {
                    write(nim.toSeq.map(
                        {case (a, b) => a.toString + "," + b.toString})
                        .mkString("\n")); close() }
                val ni: Broadcast[Map[VertexId, Double]] = sc.broadcast(nim)
                
                def vertexProgram(data: Any, niter: Int)(vid: VertexId, attr: Map[VertexId, Double], message: Map[VertexId, Double]): Map[VertexId, Double] = {
                      if (message.isEmpty) {
                        /* Nonce case concerning the first message */
                        return attr;
                      }
                      else {
                        /* Create a new message by normalizing the sum of all messages input */
                        val norm_message = normalize(message, 1.0);
                        val filtered_message = take(norm_message.filter {case (k, v) => v > threshold});
                        if (filtered_message.size == 0) {
                            /* If everythind is less than the threshold keep the one with biggest value */
                            if (niter < maxSteps - 1) {
                                return Map(message.maxBy(_._2)._1 -> 1.0 * ni.value(vid))
                            }
                            else{
                                return Map(message.maxBy(_._2)._1 -> 1.0)
                            }
                        }
                        else if (filtered_message.size == norm_message.size){
                            if (niter < maxSteps - 1) {
                                val w = ni.value(vid)
                                return filtered_message.transform {case (k, v) => (v*w)};
                            }
                            else{
                                return filtered_message
                            }
                        }
                        else{
                            if (niter < maxSteps - 1) {
                                return normalize(filtered_message, ni.value(vid))
                            }
                            else{
                                return normalize(filtered_message, 1.0)
                            }
                        }
                     }
                    }
                return PregelCME(initGraph, initialMessage,
                                 initialData, maxIterations = maxSteps,
                                 sc, activeDirection)(vprog = vertexProgram, sendMsg = sendMessage,
                                 mergeMsg = mergeMessage, getData = nonce, printF)        
                }
            else{
                def vertexProgram(data: Any, niter: Int)(vid: VertexId, attr: Map[VertexId, Double], message: Map[VertexId, Double]): Map[VertexId, Double] = {
                    if (message.isEmpty) {
                        /* Nonce case concerning the first message */
                        return attr;
                    }
                    else {
                        /* Create a new message by normalizing the sum of all messages input */
                        val norm_message = normalize(message, 1.0);
                        val filtered_message = take(norm_message.filter {case (k, v) => v > threshold});
                        if (filtered_message.size == 0) {
                            /* If everythind is less than the threshold keep the one with biggest value */
                            return Map(message.maxBy(_._2)._1 -> 1.0)
                        }
                        else if (filtered_message.size == norm_message.size){
                            return filtered_message
                        }
                        else{
                            return normalize(filtered_message, 1.0);
                        }
                    }
                }
                return PregelCME(initGraph, initialMessage,
                                 initialData, maxIterations = maxSteps,
                                 sc, activeDirection)(vprog = vertexProgram, sendMsg = sendMessage,
                                 mergeMsg = mergeMessage, getData = nonce, printF)        
            }
        }
    case Some(params) => {
        val initGraph = g.mapVertices({ case (vid, _) => Map(vid -> 1.0)}).cache()
        val nf = params._3 * initGraph.numVertices
        val mup = nf * params._1
        val sigmap = 2*pow((params._2 * nf), 2)
        val ub = params._4.toLong 
        val weight = (cluster_size: Long) => exp(-pow(cluster_size-mup, 2)/sigmap)

        val initialData: Map[VertexId, Long] = g.vertices.map({case (vid, _) => (vid, 1L)}).collect.toMap

        def mapReducer(data: Broadcast[Map[VertexId, Long]])(g: Graph[Map[VertexId, Double], ED]): Map[VertexId, Long] ={
            /* Store cluster size */        
            return g.vertices.flatMap({case (vid, vm) => vm.map({case (id, _) => (id, 1L)}).toSeq}).aggregateByKey(0L)(_ + _, _ + _).collect.toMap
        }

        if (with_ew){
            /* Calculate normalized weigths for all nodes based on the degree count and triangle count */
            val nim: Map[VertexId, Double] = NI.run(g, sc)
            new PrintWriter(base_filename + "_NIweights.csv") {
                write(nim.toSeq.map(
                    {case (b, a) => a.toString.trim + "," + b.toString.trim})
                    .mkString("\n")); close() }
            val ni: Broadcast[Map[VertexId, Double]] = sc.broadcast(nim)
                  
            def vertexProgram(data: Broadcast[Map[VertexId, Long]], niter: Int)(vid: VertexId, attr: Map[VertexId, Double], messag: Map[VertexId, Double]): Map[VertexId, Double] = {
                 val message = if (ub >= 1L){
                                 messag.filter({case (k, _) => attr.contains(k) || data.value(k) <= ub})
                               }else{
                                 messag
                               }
                  if (message.isEmpty) {
                       /* Nonce case concerning the first message */
                       return attr;
                  }
                  else {
                    /* Create a new message by normalizing the sum of all messages input */
                    val norm_message = normalize(message, 1.0);
                    val filtered_message = take(norm_message.filter {case (k, v) => v > threshold});
                    if (filtered_message.size == 0) {
                        /* If everythind is less than the threshold keep the one with biggest value */
                        if (niter < maxSteps - 1) {
                            return Map(message.maxBy(_._2)._1 -> 1.0 * ni.value(vid))
                        }
                        else{
                            return Map(message.maxBy(_._2)._1 -> 1.0)
                        }
                    }
                    else{
                        /* After filtering scale based on cluster size and normalize */
                        val cw_message = filtered_message.transform((k, v) => (v * weight(data.value(k))))
                        if (niter < maxSteps - 1) {
                            return normalize(cw_message, ni.value(vid));
                        }
                        else{
                            return normalize(cw_message, 1.0);
                        }
                    }
                 }
                }
            return PregelCME(initGraph, initialMessage,
                             initialData, maxIterations = maxSteps,
                             sc, activeDirection)(vprog = vertexProgram, sendMsg = sendMessage,
                             mergeMsg = mergeMessage, getData = mapReducer, printF)        
            }
        else{
            def vertexProgram(data: Broadcast[Map[VertexId, Long]], niter: Int)(vid: VertexId, attr: Map[VertexId, Double], messag: Map[VertexId, Double]): Map[VertexId, Double] = {
                val message = if (ub >= 1L){
                                messag.filter({case (k, _) => attr.contains(k) || data.value(k) <= ub})
                              }else{
                                messag
                              }
                if (message.isEmpty) {
                    /* Nonce case concerning the first message */
                    return attr;
                }
                else {
                    /* Create a new message by normalizing the sum of all messages input */
                    val norm_message = normalize(message, 1.0)
                    val filtered_message = take(norm_message.filter {case (k, v) => v > threshold});
                    if (filtered_message.size == 0) {
                        /* If everythind is less than the threshold keep the one with biggest value */
                        val ke = message.maxBy(_._2)._1
                        return Map(ke -> 1.0)
                    }
                    else{
                        /* After filtering scale based on cluster size and normalize */
                        val cw_message = filtered_message.transform((k, v) => (v * weight(data.value(k))))
                        return normalize(cw_message, 1.0);
                    }
                }
            }
            return PregelCME(initGraph, initialMessage,
                             initialData, maxIterations = maxSteps,
                             sc, activeDirection)(vprog = vertexProgram, sendMsg = sendMessage,
                             mergeMsg = mergeMessage, getData = mapReducer, printF)        
        }
    }
    }
  }
}

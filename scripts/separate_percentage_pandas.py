from __future__ import print_function
import sys, os, tempfile, warnings
import pandas as pd
from six import iteritems
import numpy as np

if len(sys.argv) != 5 and len(sys.argv) != 4:
    print("Usage: python " + sys.argv[0] +  " edges.csv essential_nodes.csv [outdir] percentage_of_total_file")
    sys.exit(1)

if len(sys.argv) == 5:
    edge_file, essential_nodes, outdir, percentage = sys.argv[1:]
else:
    edge_file, essential_nodes, percentage = sys.argv[1:]
    outdir = ''

percentage = float(percentage)
if percentage < 0 or percentage >= 100:
   print("percentage must be an float inside [0, 100)")
   sys.exit(1)

m, ext = os.path.splitext(edge_file)
_, filename = os.path.split(m) 
if percentage.is_integer():    
    outfile_name = filename + "_" + str(int(percentage)) + "p" + ext    
else:
    outfile_name = filename + "_" + str(int(percentage)) + "p" + ext

outfile = open(os.path.join(outdir, outfile_name), 'w')

rest = tempfile.NamedTemporaryFile(mode='w+')
with open(essential_nodes) as f:
    en = {line.split(',')[0] for line in f}

def iterate(iterable, nl, prefix):
    try:
        from tqdm import tqdm
        print(prefix)
        return tqdm(iterable, total=nl,
                    unit='l', unit_scale=True, unit_divisor=1000)
    except ImportError:
        return iterable


edf = pd.read_csv(edge_file, sep=",", header=None, names=["src", "dst", "nt", "vt"], engine='c').to_dict("index")
ndf = set(pd.read_csv(essential_nodes, sep=",", header=None, usecols=[0], names=["nd"])["nd"])

listN, listS = list(), list()

nl = len(edf)
fg, rest = [], {}
for i, row in iterate(iteritems(edf), len(edf), "Separating the induced graph, from edges of the essential_nodes:"):
    if row["src"] in ndf or row["dst"] in ndf:
        fg.append(row)
    else:
        rest[len(rest)] = row


lines_out = nl - len(rest)
if lines_out == 0 :
    warnings.warn("No edges contain the desired nodes inside the edge file ..")

# Save the percentage
#import pdb; pdb.set_trace()
if percentage > 0:
    np.random.seed(42)
    new_lines = max(int(nl*percentage/100.0)-lines_out, 0)
    if new_lines > 0:
        idxs = (np.sort(np.random.choice(len(rest), min(len(rest), new_lines), replace=False))).tolist()
        for idx in iterate(idxs, len(idxs), "Random sampling the rest graph "
                                            "to reached the desired "
                                            + str(percentage) + "% percentage:"):
	    fg.append(rest.pop(idx))
    else:
        warnings.warn("Percentage already cover the induced nodes .. No new nodes.")

#import pdb; pdb.set_trace()
pd.DataFrame(fg).to_csv(outfile, header=None, sep=',', cols=["src","dst","nt","vt"], index=False)

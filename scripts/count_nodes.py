import sys
f = str(sys.argv[1])

with open(f) as f:
    data = set()
    for line in f:
        a, b = [x.strip() for x in line.split(',')[0: 2]]
        data.add(a)
        data.add(b)
    print(len(data))


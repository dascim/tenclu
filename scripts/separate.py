from __future__ import print_function
import sys

if len(sys.argv) != 4:
    print("Usage: python " + sys.argv[0] + " edges.csv essential_nodes.csv output_name")
    sys.exit(1)

edge_file, essential_nodes, output_name = sys.argv[1:]
induced = open(output_name + "_induced.csv", 'w')
rest = open(output_name + "_rest.csv", 'w')

with open(essential_nodes) as f:
    en = {line.split(',')[0] for line in f}

with open(edge_file) as f:
    for line in f:
        a, b = line.split(',')[:2]
        if a in en or b in en:
            print(line, end='', file=induced)
        else:
            print(line, end='', file=rest)

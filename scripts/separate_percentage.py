from __future__ import print_function
import sys, os, tempfile, warnings

if len(sys.argv) != 5 and len(sys.argv) != 4:
    print("Usage: python " + sys.argv[0] +  " edges.csv essential_nodes.csv [outdir] percentage_of_total_file")
    sys.exit(1)

if len(sys.argv) == 5:
    edge_file, essential_nodes, outdir, percentage = sys.argv[1:]
else:
    edge_file, essential_nodes, percentage = sys.argv[1:]
    outdir = ''

percentage = float(percentage)
if percentage < 0 or percentage >= 100:
   print("percentage must be an float inside [0, 100)")
   sys.exit(1)

m, ext = os.path.splitext(edge_file)
_, filename = os.path.split(m) 
if percentage.is_integer():    
    outfile_name = filename + "_" + str(int(percentage)) + "p" + ext    
else:
    outfile_name = filename + "_" + str(int(percentage)) + "p" + ext

outfile = open(os.path.join(outdir, outfile_name), 'w')

rest = tempfile.NamedTemporaryFile(mode='w+')
with open(essential_nodes) as f:
    en = {line.split(',')[0] for line in f}

def iterate(iterable, nl, prefix):
    try:
        from tqdm import tqdm
        print(prefix)
        return tqdm(iterable, total=nl,
                    unit='l', unit_scale=True, unit_divisor=1000)
    except ImportError:
        return iterable

# Separate
lines_rest, nl = 0, len(open(edge_file).readlines())
with open(edge_file) as f:
    for line in iterate(f, nl, "Separating the induced graph, from edges of the essential_nodes:"):
        a, b = line.split(',')[:2]
        if a in en or b in en:
            print(line, end='', file=outfile)
        else:
            lines_rest += 1
            print(line, end='', file=rest)

lines_out = nl - lines_rest
if lines_out == 0 :
    warnings.warn("No edges contain the desired nodes inside the edge file ..")

# Save the percentage
if percentage > 0:
    rest.seek(0)
    import numpy as np
    np.random.seed(42)
    new_lines = max(int(nl*percentage/100.0)-lines_out, 0)
    if new_lines > 0:
        q = (-np.sort(-np.random.choice(lines_rest,
                                        min(lines_rest, new_lines),
                                        replace=False))).tolist()
        max_line = q[0]    
        for (i, line) in iterate(enumerate(rest),
                                 max_line + 1,
                                 "Random sampling the rest graph "
                                 "to reached the desired "
                                 + str(percentage) + "% percentage:"):
           if i == q[-1]:
               print(line, end='', file=outfile)
               q.pop(-1)
           if len(q) == 0:
               break

    else:
        warnings.warn("Percentage already cover the induced nodes .. No new nodes.")

outfile.close()

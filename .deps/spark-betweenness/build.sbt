name := "spark-betweenness"

organization := "com.centrality"

version := "1.0"

scalaVersion := "2.11.8"

crossScalaVersions := Seq("2.10.5", "2.11.8")

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.2.4" % "test"

spName := "dmarcous/spark-betweenness"

sparkVersion := "2.3.0"

sparkComponents += "graphx" 

spAppendScalaVersion := true

spShortDescription := "k Betweenness Centrality algorithm for Spark using GraphX"

spDescription := """Computing k Betweenness Centrality (kBC) on arbitrary graphs using GraphX. 
                    |Uses Pregel API for k-graphlet generation and Brandes algorithm for kBC score contribution for each vertex in parallel. 
                    |Works best for graphs with small diameter.""".stripMargin

spHomepage := "https://github.com/dmarcous/spark-betweenness"

licenses += ("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0"))

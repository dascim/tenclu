// Module name
name := "ten_cent"

// Version
version := "0.0.1"

// License
licenses += "MIT" -> url("http://opensource.org/licenses/MIT")

// Scala version
scalaVersion := "2.11.12"

// Specify which versions of scala are allowed
crossScalaVersions := Seq("2.10.5", "2.11.12")

//Spark version
sparkVersion := "2.3.0"

// Spark dependencies
sparkComponents ++= Seq("core", "sql", "mllib", "graphx", "hive")

// Dependencies for louvain
libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "3.6.0"
)


// Dependencies for merging two map on common values
libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.23"

// Dependencies for pb
/*
libraryDependencies ++= Seq(
  "com.github.nscala-time" %% "nscala-time" % "2.2.0",
  "org.scalatest" %% "scalatest" % "2.2.5" % "test",
  "jline" % "jline" % "2.13"
)*/

// Credentials for spark package
credentials += Credentials(Path.userHome / ".ivy2" / ".sbtcredentials")

// Specify multiple scala versions are published in package release
spAppendScalaVersion := true

// Disable UnitTest parallel executions for spark testing package
parallelExecution in Test := false

